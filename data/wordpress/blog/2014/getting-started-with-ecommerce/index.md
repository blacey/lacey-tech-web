---
title: "Getting Started With An E-Commerce Website"
date: "2014-04-28"
categories: 
  - "ecommerce-development"
tags: 
  - "ecommerce"
---

In the current market, it has become essential for online businesses to think about e-commerce websites to help generate sales. E-commerce websites are becoming popular thanks to their minimal running costs and global exposure.

For an e-commerce website to be a success it needs a [professional website design](/services/website-design/ "Professional Website Design"), good Search Engine Optimisation and [well written content](/shop/content-writing/) on your products within your store to turn a visitor into a customer.

A professionally designed product page combined with good on-page search engine optimisation will contribute towards the success of your **E-Commerce website**.

Your website visitors (your potential customer) should always be your top priority. Your website should be well presented and organised so the visitor can easily find the information they need from any page in the website.

## What E-Commerce solutions are available?

There are plenty of other E-Commerce systems out there to choose from and it can be hard to know which one is going best for your business. Below are just a handful of E-Commerce systems you can choose from.

![Magento E-Commerce Logo](images/magento-logo.jpg)

![WooCommerce Logo](images/woocommerce-logo.jpg)

![VirtueMart E-Commerce Logo](images/virtuemart-logo.jpg)

![OpenCart E-Commerce Logo](images/opencart-logo.jpg)

![BigCartel E-Commerce Logo](images/bigcartel-logo.jpg)

![CubeCart E-Commerce Logo](images/cubecart-logo.jpg)

## Features to look out for when choosing an E-Commerce framework

When you start using an E-Commerce framework its very hard to move to a new framework without losing access to previous orders, invoices etc so its important that the framework you choose meets your current and future needs. Most E-Commerce frameworks allow you to extend functionality using extensions or plugins so you can add new functionality to your store.

### What features should you look out for?

- Product categorisation
- Shopping Cart customisation
- Mobile friendly E-Commerce features
- The ability to pick and choose different payment gateways such as PayPal, RealEx, SagePay etc
- Easily add and use an SSL certificates if your storing customer details on your website
- Add/extend the functionality to your [E-Commerce website](https://http://laceytech.local/pwa/services/ecommerce-websites/) using extensions or plugins
- The E-Commerce framework should be able to provide coupon codes for single products, a range of products, percentage or fixed discount off your shopping cart etc
- Import / Export functionality
- Regular backup options
- Easily add Social Sharing options like share to Facebook, Twitter, Google+ etc
- Support for Multilingual language selection (Important if your trading in multiple countries)

## Important thing to consider when starting an E-Commerce website

### Do you pay monthly and are there any restrictions to be aware of?

There are plenty of E-Commerce systems available that are offered as a service at a monthly or yearly fee. These systems often impose a tiered pricing structure with limits on how many products you can sell (depending on package).

The important thing to note here is if you're paying for a monthly E-Commerce service you won't own the website - you are merely paying for the use of the service.

### Do you own the website?

If you use self-hosted E-Commerce solutions like WooCommerce or Magento then you own the website. You will find this is a bigger initial investment as you have to pay a developer to build your store but once it's built its yours.

This gives you a better foundation for you to build your online store as there won't be any limitations on how many products you can sell. As your business grows you can easily extend the website with additional functionality and features that might not be available on pay-monthly [E-Commerce solutions.](https://http://laceytech.local/pwa/services/ecommerce-websites/)

### Use SSL to secure customer data

When developing an E-Commerce website, you need to pay attention to the privacy and security of the website and its hosting environment. If you are storing customer details (Billing address, shipping address, credit card details etc) on the website then you should use an SSL certificate to encrypt access to customer login areas of the site.

### Use Rich Snippet markup to get greater exposure in search engines

Every company hopes to expand its online operation by developing a strong website presence, so you can couple your **E-Commerce website** with search engine optimisation and online marketing to get the best results possible.

In a previous article I explained how you can use rich snippets to improve how your website is shown in search engines results. In terms of E-Commerce; [rich snippet markup](https://http://laceytech.local/pwa/blog/improve-your-website-with-rich-snippets/) can be added to your product pages so the product image, reviews and price can be shown in the search results.

Other E-Commerce systems can be downloaded and installed on a self-hosted basis. You would still need to pay someone to design a website theme and setup the website for you but this is often cheaper in the long run.

You would still have to pay monthly for [website hosting](https://http://laceytech.local/pwa/services/website-hosting/) but there would be no limitation on how many pages or products you can have. This means there is a fixed price no matter how big or small your store happens to be.
