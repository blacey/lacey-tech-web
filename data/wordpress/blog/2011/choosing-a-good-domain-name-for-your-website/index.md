---
title: "Choosing Domain Name For Your Website"
date: "2011-09-20"
categories: 
  - "website-hosting"
tags: 
  - "domain-names"
  - "domains"
  - "web-hosting"
---

Choosing the right domain name for your website can be difficult so I've compiled a list of top tips on how to choose an effective domain name for your website.

If you need assistance choosing or registering a domain name for your business please [get in touch](/contact) and I can assist.

## Brainstorm 5 Top Keywords

When you first begin your domain name search, it helps to have 5 terms or phrases in mind that best describe the domain you're seeking. Once you have this list, you can start to pair them or add prefixes & suffixes to create good domain ideas.

For example, if you're launching a mortgage related domain, you might start with words like "mortage, finance, home equity, interest rate, house payment" then play around until you can find a good match.

## Make the Domain Unique

Having your website confused with a popular site already owned by someone else is a recipe for disaster. Thus, I never choose domains that are simply the plural, hyphenated or misspelled version of an already established domain.

## Secure .co.uk and .com Domains

If you're serious about building a successful website over the long-term, you should be worried about all of these elements, and while driving traffic to a .net or .org website is fine you really need to have a .com address as your default domain if you have a business website.

Websites like 1-and-1, 123-reg and godaddy allow you to purchase domain names in bulk for a reduced price for your first year. This is a brilliant idea and well worth the investment as it ensures that nobody can setup a site using your website name.

## Make the URL easy to type

If a domain name requires considerable attention to type correctly (due to spelling, length or the use of un-memorable words or sounds) then you've already lost a segment of your branding and marketing value for your site. Your website should be easy to type, and most importantly...

## Make it Easy to Remember

Remember that word-of-mouth and Search Engine Result Pages (SERPs) rely on the ease with which the domain can be called to mind.

You don't want to be the company with the terrific website that no one can ever remember to tell their friends about because they can't remember the domain name or remember what your site is about.

This is why it is key to make sure the content on your website relates to what your website is about.

## Keep Your Domain Name Short

Short website addresses (URL's) are easy to type and easy to remember. They allow you to use more characters in the URL for your sub-pages (which appear in the search engine result pages) and shorter website addresses often fit on business cards easier.

## Create and Fulfil Expectations

When someone hears about your domain name for the first time, they should be able to instantly and accurately guess at the type of content that might be found there. That's why I love domain names like Hotmail.com, CareerBuilder.com, and AutoTrader.com.

If you need assistance choosing or registering a domain name for your business please [get in touch](/contact) and I can assist.
