---
title: "What Business Owners Need To Know About Facebook Reactions"
date: "2017-09-07"
categories: 
  - "social-media"
---

The big social media networks are ever updating, ever testing the water with new features and tools that add more nuance and depth to the ways that we communicate online. Individual users adopt these new features with ease, incorporating new methods of communication that allow their conversations to organically evolve in new directions.

If you run a business with a presence on social media, you have to learn to evolve with them. That organic nature of communication is what makes you fit the landscape. If your methods are too stale or outdated, it creates an illusion that you’re not really in touch with the community and how they talk to one another.

Facebook Reactions are one of the latest examples of a small addition to the online vocabulary of the userbase that can make big changes in how they experience it. Here, we’re going to be looking at if your business should be using them, when they should, and how to better understand how they fit in the marketing and branding landscape you’re trying to create.

## What are Facebook Reactions?

Facebook Reactions are essentially an "extension of the like button", as [app and social media writer Karissa Bell](http://mashable.com/2015/10/08/facebook-reactions-how-to/#C3D81zBzVaqP) puts it. Since they were introduced in late-2016, you have probably seen them already. They are emojis built into the Like button system.

When you hold on the button for a long time instead of just pressing it, you are given access to a variety of reactions, ranging from "love" and "haha" to "sad" and "anger", each with Facebook's trademark visual design quality.

For businesses, you are mostly going to be on the receiving side of these emojis. You can use Facebook Insights to check not only how many reactions you get on individual posts, but how many you get in the long-run, when you get them, and even what reactions the page itself is getting.

## Insight Through Emojis

Before we go into whether or not you should actually use these emoji Reactions, yourself, let’s look at some of the passive benefits they can offer business owners. Social media is considered an outreach tactic for branding, first and foremost.

It’s another platform that allows you to get content, announcements, calls-to-action, and more out to an audience. In the case of Facebook, it’s a huge audience, one that every business owner should be capitalising on.

However, making the right choices in marketing isn’t just about platform. When you post on Facebook, you are almost always guaranteed to get more people reading a post and commenting on it.

> **Regardless of how marketing evolves, the emphasis will always revolve around knowing your customer**
> 
> [Marc Gubert](http://marcguberti.com/2017/02/4-ways-to-use-social-media-understand-your-customers/)

In the past, Likes were used by businesses as an arbitrary measure of how many readers interacted with the post in any meaningful way. You knew they were there, but you didn’t really know what they thought about or felt about a post unless they commented. With Reactions, you get a little more insight into what reactions, exactly, your content inspires.

Likes still serve the same nebulous function as ever, but when it comes to marketing, emotional pull is powerful. A laugh or an expression of surprise are more powerful than a passing glance. Reactions give you the chance to see what emotions your content inspires.

If you can spot what pleases, amuses, or shocks your audience, you can better craft content with more of the same emotional impact. The more emotional impact, the more likely that piece of content, and whoever produced it, is going to stay in the head of the audience.

## About Facebook's Algorithm

You should be aiming for that emotional impact for more reasons than mentioned above. Facebook's algorithms, how it decided what to put on any user's news feed, have been hard to understand. Most realise that they actively suppress a business’s messaging, which works against your efforts.

Businesses and other community pages fight against this by getting more likes, comments, and such on their content. That is why so many use calls-to-action such as "like if you agree". They’re not just getting the reader involved, they’re taking control of the algorithm.

Facebook Reactions are now a powerful tool in wrestling that algorithm. A Love, for instance, is more powerful than a like. The more you get visitors and readers to react to your content, the more likely your content is to appear on their feed. Similarly, a funny post that gets more “Ha ha” reactions does the same thing. Reactions gives business more reasons to use emotive content.

Engaging, emotional content has always been an important part of online marketing. In explaining the role of emotion in branding, [Jeremy Ellens](https://www.entrepreneur.com/article/249121) has a simple rule to dictate the content you produce: “benefits first, features second”. The real visibility aspect of gaining Facebook reactions only strengthens the link between emotions and marketing that you should have always been using.

## Stop Buying Facebook Engagement

Like-farming is the name given to the practice mentioned above, where pages would create content with a call-to-action to like posts. Nowadays, not only is that practice less valuable in helping you rise to the top of a news feed, it is a tactic known by most Facebook users and often ignored on sight.

You have to be sensible about how you entice reactions. Beyond inspiring organic interactions from your audience, you can create posts that better promote them. For instance, you can use the opportunity to teach fans of your page about how to use these React functions.

Most users already know about them, but it’s a good way to make sure that your audience is making use of the tools that can benefit you. You can even choose to be direct and honest, telling them that how they react helps you determine which content is most helpful, most entertaining, and most relevant to them.

You can also organise polls where different reactions are used to tally results. It gives the user a chance to interact quickly with a post, sharing their opinion or thoughts, while also feeding in the visibility and reach of your page.

As always, eliciting real emotional reactions should take priority over creating the social media reactions. They fulfil the same purpose while also focusing on content that priorities the user's real engagement.

However, "reaction farming" might very well have a small place in your social media strategy. It is when you overuse it that you come become in danger of putting off more of your audience than you win over.

## Winning Real Customer Reactions

There is a real benefit of getting more Facebook Reactions, as we have stated, but real emotional reactions have greater long-term benefit for the business. So, how do you get it? Using different media to engage users on different levels has always been an important part of marketing.

That’s why, beyond using images, you should include quick, interesting, and engaging videos in your social media marketing campaign. There's data that provides some pretty conclusive evidence on how important video marketing truly is.

> Videos received a massive 60% more reactions than image posts.
> 
> [Quinlty.com](https://www.quintly.com/blog/2016/09/facebook-reactions-study-new-reactions-picking-up-pace/)

Getting viewers to click on your video is initially more difficult than getting them to look at a picture. There are certain factors, like keeping it under two minutes long, that can make an impact on how likely they are to view it. However, if you do get them to view it, they tend to elicit stronger emotional reactions.

The point is that you need to focus on the emotional reaction as opposed to just informative content if you want to win Reactions. As stated above: "benefits first, features second". Videos and pictures that share a story are going to get more reactions than content with the same aim but focused on a more fact-sharing angle.

## Get In Front of The Story

A certain pair of Facebook reactions has brought with them a surprising tool to help your business manage its reputation online, too. Negative reviews are never a good thing for a business owner to see.

If left on your Facebook page, they can drag your rating down. If they’re left on another website's listings, they risk becoming some of the first things that your audience sees when they Google your business.

Facebook Reactions gives you a way to get in front of those dangers. Sometimes, negative reviews are given not because a customer didn’t like a service, but because a service was down without any explanation or information. Pinning informative posts explaining these issues to customers at the top of your Facebook page is an important way to control the message.

Now, with Facebook Reactions, customers are still able to express their frustration with Angry or Sad emojis, which makes them much less likely to do it in a review instead. You just have to make sure you’re as proactive about getting the pinned post out there as possible.

## The Shared Language

Now we start looking into whether businesses should use Facebook reactions, themselves and how they should. In part, it depends on the business and what kind of audience you are marketing to. There is no doubt that businesses have the potential to win a lot of attention and life-long fans by focusing on informal, conversational social media marketing.

**Facebook Reactions** are part of a shared language that you can use to establish that sense of community and informality with your users. Already, business pages and community pages are using videos implanting the emojis as well as just encouraging users to use them.

The danger, as Wendy’s discovered, is when that informality goes too far. The inclusion of a [controversial meme on their Twitter](https://www.cnbc.com/2017/01/04/wendys-saucy-tweets-are-hit-and-miss-on-social-media.html) created a backlash that later undoubtedly hurt the brand’s perception in the short-term. Failing to properly gauge the appropriate lines of dialogue for your own social media interactions puts you in danger of ostracising your audience, rather than including them further.

Find the brand's communicative strengths and play to them. For instance, in business 2 business industries, an informative, professional manner of communication is important for creating the distinction that you are reliable and serious about your work. Aiming for informal, conversational content or banter with your audience can work against that image.

## Conclusion

Hopefully, the points above make it much clearer on just how and whether your business should be making use of Facebook reactions. Regardless of what you do with them, they offer not only a way to gain more visibility on the social media platform, but an opportunity to get more insight on how your customers react to your content. The greater your insight of them, the greater your ability to keep creating content that brings out a strong emotional response.

They can also be used as part of the shared language that you form with your customers. Some brands will benefit more from getting “on the level” with their audience and breeding an air of familiarity. However, every business owner and social media marketing manager should be careful about how far they go down that road.

It can create branding inconsistencies or create entirely the wrong kind of appeal for the audience you want to win. When used with abandon, it also increases the risk that your business will veer into inappropriate territory that can become a mini-PR nightmare, rather than the effective viral marketing strategy you wanted.

It’s up to you to decide which ways your business should make use of Facebook Reactions. There is no doubt, however, that they have become a valuable tool in social media marketing. If you don’t take them seriously, your page won’t see the levels of interaction and growth that your competition might be enjoying.
