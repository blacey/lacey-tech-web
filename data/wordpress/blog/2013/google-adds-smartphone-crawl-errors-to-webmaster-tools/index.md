---
title: "Google Adds Smartphone Crawl Errors To Webmaster Tools"
date: "2013-12-05"
categories: 
  - "seo"
tags: 
  - "seo-2"
---

Last night Google announced that they've added a new feature to Webmaster Tools that will allow search engine specialists and Web developers to track smartphone crawl errors. Google's recent algorithm updates focus on improving the user experience for people using their search engine.

They have made significant changes to make sure the websites that appear at the top of the search results have quality content and the sites are free of errors. If you click to view a site that is at the top of page 1 and you can't view the content due to a site error then Google will eventually down-rank that site accordingly.

> "Some smartphone-optimized Websites are misconfigured in that they don't show searchers the information they were seeking. For example, smartphone users are shown an error page or get redirected to an irrelevant page, but desktop users are shown the content they wanted. Some of these problems, detected by Googlebot as crawl errors, significantly hurt your Website's user experience and are the basis of some of our recently announced ranking changes for smartphone search results."
> 
> Pierre Far, Google Webmaster Analyist
> 
> Pierre Far, Google Webmaster Analyist

## Smartphone Crawl Error Report

To see the new feature simply login to Google Webmaster Tools, click on the 'Crawl' link on the left hand side then click 'Crawl Errors'. From here you will see three tabs - Web, Smartphone and Featured Phone.

![Google Smartphone crawl errors](images/Webp.net-resizeimage-9.jpg)

## How Will This Help Business Owners?

More people are using and browsing the Internet on mobile devices so its imperative that your site is error free and works on as many devices as possible. Google's latest addition to Webmaster Tools will help developers and SEO's find and fix coding errors that prevent mobile phone users from reaching their target web pages.

If your website is error free and has good quality content, there is a good chance that your site will appear higher in the search results. Regularly checking and fixing website crawl errors is one way to help improve your website's position in the search results.

If you need help improving your website's position in the search results please feel free to [get in touch](/contact "Get in touch"). You can read about how I've helped other customers over in the [SEO Case Studies](/case-studies) section.
