---
title: "Hide Email Addresses From Spam Bots In WordPress"
date: "2013-05-19"
categories: 
  - "wordpress-development"
tags: 
  - "wordpress"
  - "wordpress-filters"
---

There may be times where you need to display your email address on your website. If you add your email address on a page, you run the risk of a spam bot adding your email address to a spam list.

The result will be either a daily or weekly barrage of spam emails delivered to your email inbox. How do we solve this problem?

## Hide Email Addresses Using a Plugin

There are a few options available if you want to hide your email address from spam bots. From the WordPress dashboard hover over Plugins and click Add New. In the plugin search box you can search for these plugins and install them.

- Email Encoded Bundle - [View Plugin](https://en-gb.wordpress.org/plugins/email-encoder-bundle/)

## Hide Email Addresses Without a Plugin

This block of code will protect email addresses that are shown within your WordPress pages/posts and widgets areas - simply add the code below to your WordPress **functions.php** file.

function remove\_plaintext\_email($emailAddress) {
$emailRegEx = '/(\[a-zA-Z0-9.\_%+-\]+@\[a-zA-Z0-9.-\]+.\[a-zA-Z\]{2,4})/i';
return preg\_replace\_callback($emailRegEx, "encodeEmail", $emailAddress);
}

function encodeEmail($result) {
return antispambot($result\[1\]);
}
add\_filter( 'the\_content', 'remove\_plaintext\_email', 20 );
add\_filter( 'widget\_text', 'remove\_plaintext\_email', 20 );

### What are WordPress Filters?

Filters are functions that WordPress passes data through. They are primarily responsible for intercepting, managing, and returning data before rendering it to the browser or saving data from the browser to the database. More information on the add\_filter WordPress function can be found on the [WordPress codex](http://codex.wordpress.org/Function_Reference/add_filter).

In our case we want to search the content of our pages/posts for email addresses and apply the WordPress antispambot function before the content is shown in the browser.

### How Does The AntiSpamBot Function Work?

The WordPress antispambot function takes an email address and converts the characters to encoded HTML characters.

Web browsers will read the encoded HTML and show the email address to visitors but spam bots would be unable to see the email address on the page.

### How Can You Add An Email Address To Theme Files?

If you need to add an email address to one of your theme's files and you want to protect it from spambots then you need to call the encodeEmail function we created earlier.

<?php echo encodeEmail('hello@domain.com'); ?>

This article has a discussion on wordpress.stackoverflow.com [Read Stackoverflow Discussion](http://wordpress.stackexchange.com/questions/151768/email-obfuscation-is-antispambot-acceptable).
