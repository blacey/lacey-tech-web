---
title: "What Is FeedBurner and Why Should You Use It?"
date: "2013-02-11"
categories: 
  - "wordpress-development"
tags: 
  - "feedburner"
  - "wordpress"
---

Today we are going to introduce you to a service called **FeedBurner** and how you can use this to greatly improve your blog readership and entice visitors to return to your website.

We'll be showing you how to use Google FeedBurner to improve how you use blog post feeds. After reading this article you'll be able to:

1. Setup email subscriptions
2. Enable feed redirection
3. Easily post updates to Facebook and Twitter without using third party tools
4. Review analytics about your blog RSS feed

## How To Offer Email Subscriptions On Your Website?

This is probably the main reason why we use FeedBurner. Your average website user probably won't know what an  RSS reader is or what RSS feeds are.

![Feedburner Email Subscriptions for WordPress Websites](images/Webp.net-resizeimage-11.jpg)

With FeedBurner, you can allow them to subscribe to daily updates via email for free. Feedburner allows you to customise your emails however you want:

- You can embed the email subscription form directly into a WordPress post, page or widget using HTML they provide
- You can change the email subject, colours and fonts used in the email. If you want a more corporate feel then you can add in your logo
- You can schedule the delivery time of your daily emails
- You can offer email subscriptions in a variety of languages

## Feedburner Feed Redirection

There may come a time, when you need to move your blog to a different domain or you may have decided to move your blog from a subdomain (blog.mywebsite.com) to a subfolder (mywebsite.com/blog).

You can use a simple .htaccess redirect so if people try accessing your old feed URLs they're automatically redirected to your new feed URL but this involves access to the web server and some advanced knowledge of website development, and is certainly not for the faint of heart.

![Setting up feed redirects with Feedburner](images/Webp.net-resizeimage-12.jpg)

FeedBurner have thought of this situation and have provided a simple way of changing your RSS feed location without losing subscribers. All you have to do is change your feed settings by clicking "Edit Feed Details..." at the top, enter the new URL of your RSS feed and click save.

No need to mess with .htaccess redirects or pray for your subscribers to update their subscription URLs. Its a one step task that takes away the pain of moving your blog - although I wouldn't suggest moving it too often as this could adversely affect your websites position in the search engines.

## Publish Social Media Updates

This feature has probably the highest number of customisation options amongst other FeedBurner features. Facebook can include the thumbnail image of your WordPress posts to use in the status update to add variation to your posts.

![Use Feedburner to publish updates to Twitter and Faceook without using WordPress plugins](images/Webp.net-resizeimage-13.jpg)

This feature allows you to connect FeedBurner to your Twitter and Facebook accounts to send updates. You can customise the updates any way you want:

- You can choose to post the title / body together or separately including a link to the post
- You can add hashtags and additional text
- You can send multiple items per update and filter the updates based on certain keywords (good if you have accounts targeting different visitors)

## Review Feedburner Analytics

Being [search engine optimisation](https://http://laceytech.local/pwa/services/search-engine-optimisation/) geeks we love the ability to look at statistical data and analyse graphs but even if your not very analytical it's still important to know a little bit about your feed subscribers (aka: your target audience).

Looking at the Feedburner statistics you can see how often they visited your website for more content (return visitors), how they read your posts (via email or a social media update) along with what country/town that visitor is from.

FeedBurner allows you to track the following statistics (and more):

- How many subscribers you have on FeedBurner
- How do they get RSS updates? (using Google Reader, Microsoft Outlook or a desktop RSS application)
- Which WordPress posts do your subscribers view and which items do they click (to visit the original page)
- What countries are your subscribers living in?

We like how Feedburner allows you to download these statistics at anytime, as it allows you to import it into Microsoft Excel (or similar spreadsheet application).

From there you can refine the data and see if there are any patterns in the data that would allow you to change your website so it turns more visitors into subscribers
