---
title: "Solution - Reduce WordPress Contact Form 7 Spam Using a Honeypot"
date: "2013-09-10"
categories: 
  - "wordpress-development"
tags: 
  - "contact-form-7"
  - "honeypot"
  - "wordpress"
  - "wp-development"
---

A honeypot is an unobtrusive method of preventing web spam. Spam Bots crawl websites in search of unencoded email addresses and contact forms, which they can use to send spam emails.

We wrote an article back in May 2013, detailing how you can [protect your email address from spambots](/blog/hide-email-addresses-from-spambots-in-wordpress/) in WordPress but today we are going to show you how to add a honeypot to the popular [Contact Form 7](http://wordpress.org/extend/plugins/contact-form-7/) plugin for WordPress.

## How Can a Honeypot Reduce Spam Emails?

When a spambot finds a website form it will fill out all the fields with spam content and submit the form. Visitors to your website will only fill out form fields that are visible so when a spam bot fills out hidden fields we know the form was submitted by a bot so we can block their email from being sent. This is an unobtrusive method of preventing spam because you're not making the contact process harder for your visitors.

### Step 1: Download the Honeypot for Contact Form 7 plugin

Download the [Honeypot for Contact Form 7](http://wordpress.org/extend/plugins/contact-form-7-honeypot/) plugin from the WordPress plugin repository.

### Step 2: Upload the plugin to your website

Login to WordPress and click, "Plugins" on the left hand side. On the Install Plugins page click on, "Upload" and browse to the zip file you downloaded in step 1 and click Install Now.

![Upload the Honeypot for Contact Form 7 Plugin](images/01-upload-plugin.gif)

### Step 3: Activate the plugin

Once the plugin has been uploaded to your website you will see a link to activate the plugin. Simply click this and the plugin will be activated. Alternatively click the "Plugins" link on the left hand side of the WordPress dashboard, scroll through the list of plugins and click the activate link under the entry for "Contact Form 7 Honeypot". Once the plugin is activated we can go and add this to our contact forms.

![Honeypot WordPress plugin activated](images/02-honeypot-plugin-activated.jpg)

### Step 4: Add the Honeypot to your contact forms

First you need to click on the "Contact" link in the left hand side of the WordPress dashboard. Next you need to click the contact form you want to add the honeypot to. Under the "Generate Tag" dropdown you will see a new option called "Honeypot".

![Add honeypot field to your contact form](images/03-add-the-honeypot-to-your-contact-form.jpg)

Now that you've renamed the field you need to copy the generated code into your contact form:

![Add the new field to your contact form markup](images/04-add-honeypot-tag-to-contact-form-markup.jpg)

Now save your form and you can rest assured that you will receive fewer automated spam enquiries from bots and scripts.
