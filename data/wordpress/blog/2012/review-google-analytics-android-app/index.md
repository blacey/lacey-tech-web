---
title: "Review of the Google Analytics Android App"
date: "2012-07-03"
categories: 
  - "seo"
tags: 
  - "app-review"
  - "google-analytics"
  - "seo-2"
---

The Google Analytics Android App is free for iOS and Android. The app links up with your Google Analytics account and allows you to view statistics about your website and its visitors easily.

The Google Analytics service allows you to find a variety of information about your website, including:

- What keywords have people typed into a search engine to find your site
- What operating system (Windows, Mac, Linux , Unix etc) have my visitors used when browsing my site
- How long have visitors stayed on my site and how many pages did they view during their visit
- What social media site did my visitors come from when viewing my site
- How many visitors used a mobile device to view my website
- What is the gender distribution that has view my content

Google have recently released an application on Google Play for Android users that will allow you to view your website analytics from your mobile phone.

## Google's getting social

Over the last year we've seen Google adding social media engagement as a ranking factor for websites. We've seen them going head to head with Facebook with the launch of Google+ and we've seen social media reports being added to its updated Google Analytics web service earlier this year.

We've seen Google showing more of an interest in social media and mobile analytics and their recent Android app will make it easier for users to view Google Analytics data from mobile devices.

The Android app is a stripped-down version of the full desktop Google Analytics service. It shows you Google Analytics new real-time data (currently in beta), allowing you to view active traffic on your site, which is good, but I would have liked to see more time spent on making the mobile app as good as their current website version.

## The Dashboard

The Dashboard lets you view graphs of your sites daily unique visitors and goal conversion rates, from one day to six months. Those are the default graphs, but you can also chart other metrics, such as e-commerce, goals, gauge tracking (including bounce rate and page views) and more.

![Google Analytics Android App Dashboard](images/google-analytics-android-app-dashboard.jpg)

You can swipe to the right of your Android's screen to view the Automatic Alerts page, where you'll find stats on your site's overall bounce rate, average visit duration, country visits, percentage of new visits and other data. There's also a Custom alerts section.

This is all helpful, and the Google Analytics app for Android is a nice first effort. But we would love to see more detailed information, such as keywords, operating systems, browsers used and the ability to compare periods, for starters. And the app doesn't really make good use of Android tablets' screen real estate.

## The Verdict

Overall I would say the Android app gives users the basic information needed about their site, which is what you would expect from the initial release. I wouldn't get too excited about the real-time statistics as this takes a good few minutes to initialise, and in some cases all you see is, "Loading..." (even after 5 minutes waiting).

I would have liked to see more of the basic information being available such as the visitors country, their operating system and keywords used to find your site, and all of these have been left out of the app at this stage.

<table><tbody><tr><th>Ease of use</th><td>7 / 10</td></tr><tr><th>Cost</th><td>10 / 10 (Its Free!)</td></tr><tr><th>Features included</th><td>5 / 10</td></tr><tr><th>Overall</th><td>6 / 10</td></tr></tbody></table>
