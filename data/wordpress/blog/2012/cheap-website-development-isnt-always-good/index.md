---
title: "Beware: Cheap Website Development Costs More"
date: "2012-01-15"
categories: 
  - "development"
tags: 
  - "web-development"
---

Budgets are stretched and you need a website so you find a cheap freelancer to build your website. Was this a good investment or something you would later live to regret?

We thought we would share some of our thoughts on cheap website development, which is often seen advertised all over the Internet.

In recent years companies throughout the UK have gone out of business or are constantly fighting to keep the books balanced. It is no surprise that businesses are looking for freelance designers and developers offering "Expert website development" at ridiculously low prices.

We work with small to medium sized businesses who need help with their website but can't afford something professional. Unlike most companies, we offer a range of payment options that make a professional website more affordable.

We thought it would be beneficial for our customers if we pointed out the potential downfalls of cheap freelancers or cheap websites in general.

## First Off - Compare Prices

- The website might be using a template design that isn't tailored to your needs
- The design or the development could be outsourced to other countries
- You might not be able to use your own domain name (mysite.com) unless you pay more
- Your website might not use website development best practices
- There may be limited support options available
- You could be restricted on how many pages or products you can have on your website
- Your website is likely to be [hosted on shared hosting](https://http://laceytech.local/pwa/services/website-hosting/) (hundreds of websites per server) so your site could be slow to load
- You might be paying monthly for your website - effectively renting it so long as you pay for the service. If you try and move to a different service you could find you'll need to start all over again!

## What Do Cheap Websites Not Include?

### Website Accessibility Standards

We looked at the generated code for the website we reviewed and it was clear that complying with website accessibility standards was not high on their priorities.

Website accessibility means that people with disabilities can use, navigate, read and interact with the website easily. The only way companies can do this is through well written, standards complaint code that makes up your website.

The W3C (World Wide Web Consortium, have a page where disabled users [share their experiences](http://www.w3.org/WAI/intro/people-use-web/stories "W3C disabled users share their experiences using the web") of using websites that don't conform to accessibility standards.

### Impact on Search Engine Rankings

Due to the quick turnaround and low cost of these websites little thought seems to have been given to following SEO ([Search Engine Optimisation](/services/search-engine-optimisation/ "Search Engine Optimisation")) techniques and practices.

This means that no matter how good your website content is, it won't rank well in search engines, resulting in fewer visitors to your website. If your a small or medium sized business then fewer visits could cripple your chances of long term successful.

### Additional Charges To Update Your New Website

Before writing this article we called a number of companies who had used a cheap site builder or went with a low priced company and ask them questions about what was included with their development package.

The majority of companies said they weren't able to update the website whenever you wanted. To make any chances to their website, they had to raise a support ticket with the company, supply them with the content and it could take up to 2 weeks for the new content to be added to website.

Other companies would allow you to submit new content to them by Email and it would be processed within 72 hours. Whilst this is more reasonable than the first scenario you would be limited to 5 content changes a month. If you asked them to change a spelling error on a page, that would count as one content amendment.

### Lack of Flexibility

Companies who churn out websites quickly and cheaply often use a slightly modified template so you'll find that the sites they produce look very similar and some might be the same.

Obviously if your a business you will need a unique corporate website design that is tailored to your needs and this is something you won't get.

### Moving To A New Hosting Company

Once your website has been built and you've had it online for a while, you might find the need or desire to move to a different hosting provider.

Sometimes this can be a difficult process if the website has been poorly developed. Some companies might not give you access to the data / files stored on your website to then transfer to a new host.

These are just a few reasons why choosing a low cost website development company might not be a wise idea in the long term.

Whenever we build a website we make it easy to update using a content management system and we give our clients the flexibility to change and update their website when it goes live.

If your interested in having a website developed or you're looking to upgrade your existing website - please feel free to [contact us](/contact/).
