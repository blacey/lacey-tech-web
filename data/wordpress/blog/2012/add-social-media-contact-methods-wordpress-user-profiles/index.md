---
title: "Add Social Media Links To WordPress User Profiles"
date: "2012-07-18"
categories: 
  - "wordpress-development"
tags: 
  - "wordpress"
  - "wordpress-filters"
---

In this WordPress tutorials you'll see how easy it is to modify contact methods that are used for user profiles. You'll also learn how to display these new fields within WordPress. This could be very useful for WordPress websites that allow anyone to register and display author information under their posts.

## Remove unwanted contact methods

WordPress by default allows you to add Yahoo IM, AIM and Jabber fields in the WordPress user profile screen. The best way to remove these fields is to add the following WordPress filter hook to `functions.php`. WordPress filter hooks are used to modify the output of an existing WordPress function.

function remove\_user\_contact\_methods( $contactmethods ) {
  unset( $contactmethods\['yim'\] );
  unset( $contactmethods\['aim'\] );
  unset( $contactmethods\['jabber'\] );

  return $contactmethods;
}
add\_filter( 'user\_contactmethods', 'remove\_user\_contact\_methods' );

## Add Social Media Links To User Profile

Add the following filter hook to `functions.php` to add the necessary fields to the user profile screen in WordPress.

function social\_media\_contact\_methods($user\_contactmethods){
  $user\_contactmethods\['twitter'\] = 'Twitter Username';
  $user\_contactmethods\['facebook'\] = 'Facebook Username';
  $user\_contactmethods\['linkedin'\] = 'LinkedIn Profile';

  return $user\_contactmethods;
}
add\_filter('user\_contactmethods', 'social\_media\_contact\_methods');

## Show Contact Methods After Your Posts

function wordpress\_post\_signature($content){
  if (is\_single() && get\_user\_meta( $user\_id, 'twitter', true) ):
    $content .= '<a href="http://www.twitter.com/' . get\_user\_meta( $user\_id, 'twitter', true) . '" target="\_blank" rel="external me">Follow ' . get\_the\_author() . ' on Twitter</a>';
  endif;  
  return $content;
}
add\_filter("the\_content", "wordpress\_post\_signature");

For more information on WordPress filter hooks please have a look at the [WordPress add\_filter codex](http://codex.wordpress.org/Function_Reference/add_filter "WordPress add_hook codex").
