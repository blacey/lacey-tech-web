---
title: "Using Twitter Bootstrap for Mobile Website Development"
date: "2015-01-29"
categories: 
  - "development"
---

Bootstrap stands as one of the most popular open source front-end frameworks on the Web. Since its official release in 2011, it has undergone several changes, and it's now one of the most stable and responsive frameworks available.

It's loved by web developers of all levels, as it gives them the capability to build a functional, attractive website design within minutes. A novice developer with just some basic knowledge of HTML and little CSS can get started with Bootstrap.

Bootstrap provides a solid foundation for any website, irrespective of project size. Even basic HTML form elements like tick boxes, radio buttons, select options, etc., have been restyled to give them a modern look.

Today's websites should be modern, sleek, responsive, and mobile first. Bootstrap helps us to achieve these goals with minimum fuss.

## Why Use Bootstrap To Build a Website?

### Powerful Grid System

Bootstrap has one of the best responsive, mobile first grid systems available. It's easy to use and flexible. It helps in scaling a single website design from the smallest mobile device to high definition displays, logically dividing the screen into 12 columns, so that you can decide just how much screen real estate each element of your design should take up.

### Rapid Development

Bootstrap comes complete with many reusable CSS and JavaScript components that can help to achieve the functionality needed in almost any kind of website. You just have to use the bootstrap HTML markup along with the JavaScript and CSS to get them working in your website. The components are responsive (mobile-friendly) out the box, which is another reason to start using them.

#### What Components Are Available?

- Glyphicons
- Dropdowns
- Button groups
- Button dropdowns
- Input groups
- Navigation menus
- Breadcrumbs
- Pagination
- Labels
- Badges
- Thumbnails
- Progress bars
- List group
- Alerts

### Browser Compatibility

Bootstrap is compatible with all modern browsers and Internet Explorer versions 8 and up. If Bootstrap's instructions are followed properly, you can create a website design that works in all these browsers. Plugins like HTML5Shiv and Respond.js come as part of Bootstrap's default template. These help in porting HTML5 elements into older non-HTML5 browsers.

### Customisation

Bootstrap offers many ways to customise its default design. You can override all of its styles (CSS) and default JavaScript behaviour. Bootstrap is even more interesting if you are a Less or Sass developer, as it includes Less and Sass customisation options. These options let you smoothly create a new template using Bootstrap.

### Open Source

[Bootstrap is an open source project](https://getbootstrap.com/), that has been released under the MIT license. Open source software is free to use, which has helped our customers who have a limited budget. Where you have access to all the source files along with the compiled version (lots of smaller files combined into one big file), it gives you the freedom to experiment.

You may want the spacing between your grid to be wider or narrower, you might want certain font sizes and colours. Everything you can think of can be customised to your requirements and its mobile-friendly without the need of any complex set up.
