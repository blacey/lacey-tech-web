---
title: "Shop"
date: "2018-10-08"
---

Lacey Tech provide products, services and resources for customers to buy online through our online store. Our team share their knowledge through E-Books, which are aimed at helping business owners learn more about certain topics.

Use the code **'BOUNCEBACK' to get 20% off** our products until September 2021 to help businesses bounce back after the Coronavirus pandemic.
