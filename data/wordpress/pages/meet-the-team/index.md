---
title: "Meet The Team"
date: "2019-03-17"
---

## Ben Lacey

**Managing Director**

Ben is the managing director of Lacey Tech Solutions, which he started after seeing companies failing to deliver on their promises. He's seen website owners picking up the pieces when developers have left them in the lurch, or companies have failed to deliver a website that fits the needs of the website owner.

He studied Web Design and Business through College and completed his development degree with a First. Several years later he left the comfort of his full-time development job and started his business with the sole aim of delivering high-quality work and [helping businesses grow](/case-studies/).

## Dom

**PHP, WordPress and Python Developer**

Dom has worked as a website developer focusing on WordPress and custom PHP frameworks. He is always looking at ways to improve and streamline online processes or systems. He is currently working on version 2 of our Industry Analysis system and will soon be working on building a custom API for a new service we will be providing.

## Caitlin

**Content Writer**

As a full-time copywriter, Caitlin has many years experience of [content writing](/shop/content-writing/) and she can write content for websites, blogs, products, brochures, eBooks and more. She has worked with us for several years and all our clients have been happy with the content produced. Sometimes the content doesn't need amending past the first draft.

## Greg

**Graphic Designer**

Greg has been a professional graphic designer with over 5 years experience and specialises in Logo Design but also loves to dabble in [Web Design](/services/website-design/)! He is a lover of dogs and sports. He also likes to donate some of his free time to walk dogs that are up for adoption at local kennels. He plays football, basketball and is a regular gym goer (well, he has a membership at least)!

## Kirsty

**Website Developer**

Kirsty obtained a Computer Science degree from The University of Stirling in 1992. After 23 years as a professional [website developer](/services/website-development/) and independent consultant. With experience across a wide range of industries and with many technologies, learning new tools is part of her daily life.

Concentrating on quality of work and directly with clients she is able to produce the solutions they need. Kirsty is a fully rounded development consultant, capable of the full project life cycle - from gathering user requirements through design and development to post-deployment bug-fixing and enhancements. In her spare time, she enjoys making music and sailing.

* * *
