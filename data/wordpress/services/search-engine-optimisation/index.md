---
title: "Search Engine Optimisation"
date: "2012-01-20"
---

Owning a business and having a website isn't enough to ensure continued growth and success. Your website needs to perform well within the search engines if you new prospect clients to find you on the Internet.

## What is Search Optimisation?

Search Engine Optimisation (SEO) is a marketing discipline focused on improving the visibility of your website in organic search engine results. Search Optimisation includes a number of tasks such as keyword research, content creation, site speed optimisation, and rank tracking to name a few.

When you search for something online you will often use a search engine like Google, Bing or Yahoo - you type what you want to search and results are shown to you based on your query. In the search results, you will see links to pages it considers relevant and authoritative. Authority is measured by analysing the number and quality of links from other web pages. Your web pages have the potential to rank in Google so long as other web pages link to them.

## SEO Services

\[service\_subpages\]

## Frequently Asked Questions

**Why do I need Search Engine Optimisation?**

The majority of web traffic is driven by the major commercial search engines, Google, Bing, and Yahoo!. Although social media and other types of traffic can generate visits to your website, search engines are the primary method of navigation for most Internet users. This is true whether your site provides content, services, products, information, or just about anything else.

**Is Search Optimisation right for my website?**

SEO is right for businesses that are looking to increase brand awareness and drive targeted visitors to their website. However, SEO is not an overnight process and is not suitable for businesses that are looking to rush a marketing strategy. PPC is suitable for more immediate results whilst SEO should be a part of a longer term strategy. The timeframe for SEO is dependent on the current status of the website (SEO health, domain authority, ranking establishment etc.) and what you want to achieve.

**How long does it take to see results from SEO?**

There is no specific time-frame for SEO. The speed in which results are seen is entirely dependent on the existing website rankings and SEO profile. Ranking changes can be seen in as little as 4 weeks after SEO fixes and improvements are carried out.

**What is the difference between Organic vs Paid Results?**

The difference between SEO and PPC is that SEO is a longer term strategy for generating larger volumes of organic traffic from search results below the paid results. PPC is a more immediate marketing channel for generating targeted website traffic (usually through the Google Adwords platform).

**Why should we invest in SEO when we can do Pay Per Click?**

Although pay per click is an effective marketing channel and can yield quick results, it can be more costly to maintain day to day visibility. Pay per click should ideally be run alongside SEO as the data from PPC can be used for an SEO campaign / towards the SEO strategy. Businesses should not neglect SEO in favour of a pay per click campaign.

**Should we run PPC alongside SEO?**

This is entirely dependent on the budget and the service / product on offer. PPC and SEO work well together as the conversion data from pay per click can be used as part of the SEO strategy. If the marketing budget is limited then we’ll make seperate recommendations for SEO or Pay Per Click.

**Why is search intent important for SEO?**

Search intent is important for search engine optimisation because the satisfaction of a users search query is exactly what high ranking websites should be doing. In order for search engines like Google to carry on delivering high quality results, they need to know that the high ranking organic results are satisfying user search queries. This all counts towards user experience and intent satisfaction.  
  
Satisfying a searchers intent is going to become one of the biggest aspects of SEO over time. As search engines move towards using analytics data to better understand how a website is catering for its audience there has never been a better time to implement SEO that creates excellent landing pages.

### Why Choose Us?

- Over 10 years' experience working in SEO and Digital Marketing
- We have a selection of [SEO Case Studies](/case-studies/) with great results
- Extensive knowledge of technical issues that impact website rankings
- There are numerous free tools available to help you track and monitor your marketing that we can setup for you
- We can audit your website and recommend improvements to increase online visibility for your website and brand

\[contact-form-7 id="9731" title="Small Contact Form For Landing Page"\]

#### Recent Search Engine Optimisation Articles
