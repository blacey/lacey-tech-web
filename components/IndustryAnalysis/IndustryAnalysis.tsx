import Image from "next/image";
import Link from "next/link";

import industryAnalysisPic from "../../public/images/hero/industry-analysis.png";

function IndustryAnalysis() {
  return (
    <section className="text-gray-600 bg-gray">
      <div className="container mx-auto flex px-5 py-20 md:flex-row flex-col items-center">
        <div className="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
          <h2 className="sm:text-2xl text-3xl mb-4 font-normal text-gray-900">
            Industry Analysis System
          </h2>
          <p className="mb-4 leading-relaxed text-gray-700">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Viverra
            quis gravida ac non. Nunc, platea netus consectetur accumsan morbi
            risus. Placerat magna ipsum semper facilisi nunc orci, orci rutrum.
            Sed in eu commodo donec nisl pharetra rutrum.
          </p>
          <p className="mb-4 leading-relaxed text-gray-700">
            Cras nunc quis eget at pharetra, consequat orci. Sagittis auctor
            elit euismod lacus, eget est. Pharetra sed magna lectus elit. A
            ipsum purus eget mauris. Maecenas tristique nulla venenatis purus
            massa iaculis viverra. Nunc arcu ac senectus quisque at donec amet.
          </p>
          <div className="flex justify-center mt-2">
            <Link href="/">
              <a className="inline-flex text-white bg-secondary border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                Learn More
              </a>
            </Link>
          </div>
        </div>
        <div className="lg:max-w-lg lg:w-full md:w-1/2 w-5/6">
          <Image
            src={industryAnalysisPic}
            className="object-cover object-center rounded"
            alt="hero"
          />
        </div>
      </div>
    </section>
  );
}

export default IndustryAnalysis;
