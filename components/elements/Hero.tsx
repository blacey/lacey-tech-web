import React from "react";
import Image from 'next/image';
import BannerImg from '../../public/images/hero/banner.jpg'

function Hero({ heroName, children }) {
  return (
    <section className={`hero ${heroName}`}>
      <div
        className="relative pt-16 pb-32 flex content-center items-center justify-center"
        style={{ minHeight: "75vh" }}
      >
        
        <Image src={BannerImg} alt="LaceyTech" layout="fill" objectFit="cover" objectPosition="center" />
        <span
            id="blackOverlay"
            className="w-full h-full absolute opacity-75 bg-black top-0"
          ></span>
        <div className="container relative mx-auto">
          <div className="items-center flex flex-wrap">{children}</div>
        </div>
      </div>
    </section>
  );
}

export default Hero;