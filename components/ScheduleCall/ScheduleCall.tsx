import Link from "next/link";

function ScheduleCall() {
  return (
    <section className="text-gray-600 bg-havelock-blue">
      <div className="container py-20 mx-auto">
        <div className="flex flex-col text-center w-8/12 mx-auto">
          <h1 className="sm:text-3xl lg:text-4xl font-normal text-white">
            Need Help With Your Project?
          </h1>
          <p className="lg:w-2/3 mx-auto my-8 leading-relaxed text-base text-white">
            Speak to our team of experts who can help advise on how to turn your
            ideas into a working system.
          </p>
          <div className="flex justify-center">
            <Link href="/">
              <a className="inline-flex text-white bg-secondary border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">
                Schedule A Discovery Call
              </a>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}

export default ScheduleCall;
