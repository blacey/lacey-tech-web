import React from 'react'

function BlogFilters() {
    return (
        <div className="form-filters">
            <div className="container">
                <form method="POST">
                    <h2>Filter Blog Posts</h2>

                    <div className="filters">
                        <div className="filter search"><input type="text" placeholder="Search..." /></div>

                        <div className="filter categories">
                            <select name="categories">
                                <option>Select Category</option>
                                <option>WordPress</option>
                                <option>Website Design</option>
                                <option>E-Commerce</option>
                                <option>Security</option>
                                <option>SEO and Marketing</option>
                                <option>Pay Per Click</option>
                            </select>
                        </div>

                        <div className="filter author">
                            <select name="author">
                                <option>Select Author</option>
                                <option>Ben Lacey</option>
                                <option>Caitlin Mackay</option>
                                <option>Dom Webber</option>
                            </select>
                        </div>

                        <div className="filter type">
                            <select name="type">
                                <option>Free Posts</option>
                                <option>Member Only Posts</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default BlogFilters
