import React from 'react'
import Image from 'next/image'
import Link from 'next/link'
import posts from '../../data/posts.json'


function BlogList() {
    console.log('Blog List Component:')
    console.log(posts)

    return (
        <section className="container blog-list">
            {!posts && <p>Sorry there are no blog posts to display.</p>}

            {posts && (
                <div className="posts">
                {
                    posts?.map( post => {
                        const url = `/blog/${post.slug}/`

                        return (
                            <article className="post" key={post.id}>
                                <h2><Link href={url}><a>{post.title}</a></Link></h2>

                                {post.featuredImage && (
                                    <div className="featured-image">
                                        <Link href={url}>
                                            <Image src={post.featuredImage} width={800} height={300} alt={post.title} />
                                        </Link>
                                    </div>
                                )}

                                {post.excerpt && (<p>{post.excerpt}</p>)}
                                <div className="meta">
                                    {post.author && (<span className="author">Written by {post.author} </span>)}
                                    {post.dateCreated && (<span className="date-created"> - {post.dateCreated}</span>)}
                                </div>
                                <Link href={url}><a className="btn btn-secondary">Read Article</a></Link>
                            </article>
                        )
                    })
                }
                </div>
            )}
        </section>
    )
}

export default BlogList