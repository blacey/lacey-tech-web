import React from "react";
import Image from "next/image";
import Navbar from "./Navbar";
import Link from "next/link";
import { useState } from "react";

function SiteHeader() {

  const [active, setActive] = useState(false);
  const handleClick = () => {
    setActive(!active);
  };

  return (
    <header className="site-header bg-primary">
      <div className="lg:container mx-auto">
        <Navbar>
          <Link href="/">
            <a className="inline-flex items-center p-2 mr-4">
              <Image
                src="/logo.png"
                alt="Lacey Tech Logo"
                width="160px"
                height="50px"
              />
            </a>
          </Link>

          <button 
            className=" inline-flex p-3 rounded lg:hidden text-white ml-auto hover:text-white outline-none"
            onClick={handleClick}
          >
            <svg
              className="w-6 h-6"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          </button>
          <div className={`${active ? '' : 'hidden'}  w-full lg:inline-flex lg:flex-grow lg:w-auto`}>
            <div className="lg:inline-flex lg:flex-row lg:ml-auto lg:w-auto w-full lg:items-center items-start  flex flex-col lg:h-auto">
              <Link href="/">
                <a className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white items-center justify-center hover:text-gray-400 ">
                  Home
                </a>
              </Link>
              <Link href="/about/">
                <a className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white items-center justify-center hover:text-gray-400 ">
                  About
                </a>
              </Link>
              <Link href="/services/">
                <a className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white items-center justify-center hover:text-gray-400 ">
                  Services
                </a>
              </Link>
              <Link href="/case-studies/">
                <a className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white items-center justify-center hover:text-gray-400 ">
                  Case Studies
                </a>
              </Link>
              <Link href="/work/">
                <a className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white items-center justify-center hover:text-gray-400 ">
                  Work
                </a>
              </Link>
              <Link href="/contact/">
                <a className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white items-center justify-center hover:text-gray-400 ">
                  Get Free Quote
                </a>
              </Link>
            </div>
          </div>
        </Navbar>
      </div>
    </header>
  );
}

export default SiteHeader;
