import Image from "next/image";

import whyChooseUsPic from "../../public/images/placeholder.svg";

function Technology() {
  const array = [...Array(10)];
  return (
    <section className="text-gray-600">
      <div className="container px-5 py-20 mx-auto">
        <div className="flex flex-col text-center w-full mb-12">
          <h1 className="sm:text-3xl lg:text-4xl font-normal mb-4 text-gray-900">
            Experts In Modern Technology
          </h1>
          <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Viverra
            quis gravida ac non. Nunc, platea netus consectetur accumsan morbi
            risus. Placerat magna ipsum semper facilisi nunc orci, orci rutrum.
            Sed in eu commodo donec nisl pharetra rutrum. Cras nunc quis eget at
            pharetra, consequat orci. Sagittis auctor elit euismod lacus, eget
            est.
          </p>
        </div>
        <div className="flex flex-wrap">
          {array.map((_) => (
            <div className="lg:w-1/5 md:w-1/3 sm:w-1/2 p-4">
              <Image
                src={whyChooseUsPic}
                className="object-cover object-center rounded"
                alt="hero"
              />
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}

export default Technology;
