import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";

import SocialLinks from "./SocialLinks/SocialLinks";
import ContactForm from "../forms/ContactForm";

function handleSubmit(event) {
  event.preventDefault();
  console.error("** Handle Submit Fired!");
}

function SiteFooter() {
  const recentProject = [
    { name: "Divan Sahab", url: "https://divansahab.com" },
    { name: "First1Right", url: "https://first1right.com" },
    { name: "Ideal Glass", url: "https://igandg.co.uk" },
    { name: "Plumbers Hampshire", url: "https://plumbers-hampshire.com" },
    { name: "Plumbing Farnham", url: "https://plumbing-farnham.com" },
    { name: "Penn Lawn Mowers", url: "https://penn-lawn-mowers.co.uk" },
    { name: "Hopkins Electrical", url: "https://hopkinselectrical.com" },
  ];
  const serviceList = [
    { name: "Search Engine Optimisation", url: "/services/search-engine-optimisation/" },
    { name: "Local Marketing Campaigns", url: "/services/search-engine-optimisation/local-seo/" },
    { name: "WordPress Development", url: "/services/wordpress-development/" },
    { name: "E-Commerce Development", url: "/services/ecommerce/" },
    { name: "Pay Per Click", url: "/services/pay-per-click/" },
    { name: "Website SEO Audits", url: "/services/search-engine-optimisation/seo-audits/" },
    { name: "Website Security Audit", url: "/product/website-security-audit/" },
  ];
  const technologyList = [
    { name: "Amazon Web Services", url: "/technologies/amazon-web-services/" },
    { name: "WooCommerce Development", url: "/technologies/woocommerce/" },
    { name: "Amazon Web Services", url: "/technologies/amazon-web-services/" },
    { name: "Javascript Development", url: "/technologies/javascript/" },
    { name: "PHP Development", url: "/technologies/php/" },
    { name: "Symfony Development", url: "/technologies/symfony/" },
    { name: "Laravel Development", url: "/technologies/laravel/" },
  ];

  return (
    <footer className="site-footer">
      <div className="bg-primary">
        <div className="container px-6 pb-4 pt-12 mx-auto">
          <div className="lg:flex">
            <div className="w-full -mx-6 lg:w-2/5">
              <div className="px-6">
                <Link href="/">
                  <a>
                    <Image
                      src="/logo.png"
                      alt="Lacey Tech Logo"
                      width="160px"
                      height="50px"
                    />
                  </a>
                </Link>

                <p className="max-w-md mt-2 text-white">
                  Lacey Tech
                  <br />
                  Briarleas Court,
                  <br />
                  Farnborough,
                  <br />
                  Hampshire,
                  <br />
                  GU14 6HL
                  <br />
                  <br />
                  Call Us: <a href="tel:01252518233">01252 518233</a>
                </p>

                <SocialLinks />
              </div>
            </div>

            <div className="mt-6 lg:mt-0 lg:flex-1">
              <div className="grid gap-6 sm:grid-cols-3">
                <div className="recent-projects">
                  <h3 className="uppercase text-white">Recent Projects</h3>
                  {recentProject.map(function(d, key){
                      return (<Link href={d.url} key={key}><a target="_blank" className="block mt-2 text-sm text-gray-100 hover:underline">{d.name}</a></Link>)
                  })}
                </div>
                <div className="our-services">
                  <h3 className="uppercase text-white">Services</h3>
                  {serviceList.map(function(d, key){
                      return (<Link href={d.url} key={key}><a target="_blank" className="block mt-2 text-sm text-gray-100 hover:underline">{d.name}</a></Link>)
                  })}
                </div>
                <div className="technology-used">
                  <h3 className="uppercase text-white">Technology Used</h3>
                  {technologyList.map(function(d, key){
                      return (<Link href={d.url} key={key}><a target="_blank" className="block mt-2 text-sm text-gray-100 hover:underline">{d.name}</a></Link>)
                  })}
                </div>

              </div>
            </div>
          </div>

          <hr className="h-px my-6 bg-gray-300 border-none dark:bg-gray-700" />

          <div>
            <p className="text-center text-gray-200">
              Website &copy; Lacey Tech 2005- 2021
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default SiteFooter;
