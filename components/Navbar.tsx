import React from "react";

function Navbar({ children }) {
  return (
    <>
      <nav className='flex items-center flex-wrap p-3 '>
        {children}
      </nav>
    </>
  );
}

export default Navbar;
