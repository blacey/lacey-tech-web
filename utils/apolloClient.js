
import { ApolloClient, InMemoryCache } from '@apollo/client'

const apolloClient = new ApolloClient({
    cache: new InMemoryCache(),
    uri: `https://graphql.contentful.com/content/v1/spaces/b6ohdqgzdbw6`,
    headers: {
        'Authorization': `bearer RMcaGqk6TzERns4Z-krHowzP0iikSOlt6ACB5PWB_UA`
    }
})

export default apolloClient