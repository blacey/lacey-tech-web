import { Formik } from 'formik'

const ContactForm = () => (
  <>
    <Formik
      initialValues={{ firstName: '', lastName: '', email: '', phone: '', message: '' }}

      validate={values => {
        const errors = {}

        // First Name
        if (!values.firstName) {
          errors.firstName = 'Required'
        }

        // Last Name
        if (!values.lastName) {
          errors.lastName = 'Required'
        }          

        // Email
        if (!values.email) {
          errors.email = 'Required'
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
          errors.email = 'Invalid email address';
        }

        // Phone
        if (!values.phone) {
          errors.phone = 'Required'
        }

        if (!values.message) {
          errors.message = 'Required'
        }

        return errors; 
      }}

      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          console.log(JSON.stringify(values, null, 2));

          // Send Email with SendGrid
          const sgMail = require('@sendgrid/mail')
          sgMail.setApiKey("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")

          const email = {
              to: 'ben.lacey57@gmail.com',
              from: 'ben@laceytechsolutions.co.uk',
              subject: 'Sending with SendGrid is Fun',
              text: 'and easy to do anywhere, even with Node.js',
              html: '<strong>and easy to do anywhere, even with Node.js</strong>',
          }

          sgMail.send(email)
          .then(() => {
              console.log('Email has Been Sent')
          })
          .catch((error) => {
              console.error("Email Not Sent - " & error)
          })

          setSubmitting(false)
        }, 400);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form onSubmit={handleSubmit}>          
          <div className="form-group">
            <div className="form-row">
              <div className="col">
                <input
                  type="text"
                  name="firstName"
                  className="form-control"
                  placeholder="First Name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.firstName}
                />
                {errors.firstName && touched.firstName && errors.firstName}
              </div>

              <div className="col">
                <input
                  type="text"
                  name="lastName"
                  className="form-control"
                  placeholder="Last Name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.lastName}
                />
                {errors.lastName && touched.lastName && errors.lastName}
              </div>
            </div>
          </div>

          <div className="form-row">
            <div className="col">
              <div className="form-group">
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  placeholder="Email Address"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                />
                {errors.email && touched.email && errors.email}
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <input
                  type="tel"
                  name="phone"
                  className="form-control"
                  placeholder="Phone Number"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phone}
                />
                {errors.phone && touched.phone && errors.phone}
            </div>
          </div>
        </div>

        <div className="form-group">
          <textarea
            name="message"
            className="form-control"
            placeholder="How Can We Help You?"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.message}
          />
          {errors.message && touched.message && errors.message}
        </div>

        <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Send Message</button> 
      </form>
      )}
    </Formik>
  </>
);
 
export default ContactForm;