// Dependencies
import Head from "next/head";
import React from "react";
import Link from "next/link";

import { AiFillCheckCircle } from "react-icons/ai";


// Components
import Testimonials from "../components/Testimonials/Testimonials";
import WhyChooseUs from "../components/WhyChooseUs/WhyChooseUs";
import IndustryAnalysis from "../components/IndustryAnalysis/IndustryAnalysis";
import Technology from "../components/Technology/Technology";
import ScheduleCall from "../components/ScheduleCall/ScheduleCall";
import PastClientStories from "../components/PastClientStories/PastClientStories";

// Page Elements
import Hero from "../components/elements/Hero";
import PageContent from "../components/elements/PageContent";

// Styles
import "../styles/Home.module.scss";

export default function Home() {
  // const ratingStyle = {
  //   rating: 3,
  // };

  const whyChooseUsList = [
    "We help business owners with technical set ups and monitoring so they can focus on running their business",
    "Our 15 years experience has allowed local businesses to get more sales enquiries from their websites",
    "Our team help businesses track their marketing efforts to know what marketing channel is driving sales",
    "Get a no-obligation website audit to see how we can improve your business website"
  ]
  return (
    <>
      <Head>
        <title>Award Winning Website &amp; Marketing Agency Farnborough</title>
        <meta
          name="description"
          content="Lacey Tech is an award winning website and marketing company who are based in Farnborough, Hampshire. They help small to medium sized companies and has years of success stories to share."
        />
      </Head>

      <Hero heroName="home">
        <div className="w-full lg:w-3/4 px-4 mx-auto text-center">
          {/* <div className="Stars" style={ratingStyle as any}></div> */}
          <h1 className="text-white font-normal text-5xl">
            Making Websites Great Again!
          </h1>
          <p className="mt-4 text-lg text-gray-300 clear-both">
            Transform your website into a lead generation machine.
          </p>

          <div className="rounded-md shadow mt-5 flex">
            <Link href="/contact">
              <a
                className="mx-auto w-auto items-center justify-center px-8 py-3 bg-secondary border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10"
              >
                Schedule Discovery Call
              </a>
            </Link>
          </div>
        </div>
      </Hero>

      <PageContent>
        <Testimonials template="slider" />

        <WhyChooseUs />
        <IndustryAnalysis />
        <Technology />
        <ScheduleCall />
        <PastClientStories />

        <section className="text-gray-600 body-font">
          <div className="container px-5 py-24 mx-auto flex flex-wrap items-center">
            <div className="lg:w-3/5 md:w-1/2 md:pr-16 lg:pr-0 pr-0">
              <h1 className="title-font font-medium text-3xl text-gray-900">Why Choose Us?</h1>
              {whyChooseUsList.map(function(d, idx){
                return (<p className="flex items-center space-x-6 mt-4 text-xl" key={idx}><AiFillCheckCircle size={43} style={{ fill: 'green' }} /><span>{d}</span></p>)
              })}
            </div>
            <div className="lg:w-2/6 md:w-1/2 bg-gray-100 rounded-lg p-8 flex flex-col md:ml-auto w-full mt-10 md:mt-0">
              <h2 className="text-gray-900 text-lg font-medium title-font mb-5">Sign Up</h2>
              <div className="relative mb-4">
                <label htmlFor="full-name" className="leading-7 text-sm text-gray-600">Full Name</label>
                <input type="text" id="full-name" name="full-name" className="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
              </div>
              <div className="relative mb-4">
                <label htmlFor="email" className="leading-7 text-sm text-gray-600">Email</label>
                <input type="email" id="email" name="email" className="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
              </div>
              <button className="text-white bg-indigo-500 border-0 py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg">Button</button>
              <p className="text-xs text-gray-500 mt-3">Literally you probably haven't heard of them jean shorts.</p>
            </div>
          </div>
        </section>

      </PageContent>
    </>
  );
}
