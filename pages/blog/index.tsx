import React from "react";
import Link from "next/link";
import Hero from "../../components/elements/Hero";
import PageContent from "../../components/elements/PageContent";
import BlogList from "../../components/Blog/BlogList";
import BlogFilters from "../../components/Blog/BlogFilters";

function Blog() {
  return (
    <>
      <Hero heroName="blog">
        <div className="w-full lg:w-3/4 px-4 mx-auto text-center">
          <h1 className="text-white font-normal text-5xl">
          Development and Marketing Blog
          </h1>
          <p className="mt-4 text-lg text-gray-300 clear-both">
          Transform your website into a lead generation machine.
          </p>
          <div className="rounded-md shadow mt-5 flex">
            <Link href="/">
              <a className="mx-auto w-auto items-center justify-center px-8 py-3 bg-secondary border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
              Subscribe To Our Newsletter
              </a>
            </Link>
          </div>
        </div>
      </Hero>

      <PageContent>
        <div className="blog-page">
          <section className="container">
            <p>
              In laboris aliqua cupidatat ad commodo occaecat. In exercitation
              eiusmod non fugiat velit labore. Tempor qui occaecat id commodo
              voluptate. Anim nostrud in qui et sint adipisicing. Cupidatat
              eiusmod consequat occaecat proident esse ex laborum excepteur eu
              consectetur culpa aliquip. Lorem esse elit do nostrud amet
              exercitation aute consectetur laboris dolore incididunt do anim
              nulla.
            </p>
          </section>

          <BlogFilters />

          <BlogList />
        </div>
      </PageContent>
    </>
  );
}

export default Blog;
